email_providers_config = YAML.load_file("#{Rails.root}/config/email_providers.yml").with_indifferent_access

EMAIL_PROVIDERS = email_providers_config[:PROVIDERS].map do |provider, config|
  provider_wrapper = "#{provider.camelize}Client".constantize
  validator = ProviderConfigValidator.new(config)

  validator.validate!

  provider_wrapper.new(config)
end
