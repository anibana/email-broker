require 'rails_helper'

RSpec.describe BaseEmailClient do
  let(:user) { 'api' }
  let(:password) { 'password' }
  let(:api_key) { 'api-key-here' }
  let(:url) { 'http://api.endpoint.com' }
  let(:payload) {
    {
      to: 'recipient@email.com',
      from: 'sender@email.com',
      subject: 'Hello user',
      message: 'Hello world!'
    }
  }

  describe '#send!' do
    context 'when user and password configuration are provided' do
      let(:client) { BaseEmailClient.new(url: url, user: user, password: password) }

      before do
        allow(client).to receive(:payload).and_return(payload)
      end

      it 'calls RestClient#post with the correct parameters' do
        expected_url = "https://#{user}:#{password}@#{url}"
        expected_headers = {}

        expect(RestClient).to receive(:post).with(expected_url, payload, expected_headers)

        client.send!(payload)
      end
    end

    context 'when api_key is provided' do
      let(:client) { BaseEmailClient.new(url: url, api_key: api_key) }

      before do
        allow(client).to receive(:payload).and_return(payload)
      end

      it 'calls RestClient#post with the correct parameters' do
        expected_url = url
        expected_headers = {Authorization: "Bearer #{api_key}", content_type: :json}

        expect(RestClient).to receive(:post).with(expected_url, payload, expected_headers)

        client.send!(payload)
      end
    end
  end

  describe '#payload' do
    class OtherEmailClient < BaseEmailClient; end

    let(:client) { OtherEmailClient.new(url: url, api_key: api_key) }

    it 'raises an error when subclass is not implemented' do
      expect { client.send!(payload) }.to raise_error(NotImplementedError, '#payload method has not been implemented in OtherEmailClient')

    end
  end

end
