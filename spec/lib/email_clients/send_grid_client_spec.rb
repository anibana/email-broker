require 'rails_helper'

RSpec.describe SendGridClient do
  let(:api_key) { 'api-key-here' }
  let(:url) { 'http://api.endpoint.com' }
  let(:sender) { 'sender@email.com' }
  let(:recipient) { 'recipient@email.com' }
  let(:email1) { 'email1@email.com' }
  let(:email2) { 'email2@email.com' }
  let(:email_subject) { 'Hello user' }
  let(:message) { 'Hello world!' }

  let(:message_config) do
    {
      to: recipient,
      from: sender,
      subject: email_subject,
      message: message
    }
  end

  let(:expected_to_list) { [{email: recipient}] }

  let(:recipient_list) { {to: expected_to_list} }

  let(:expected_payload) do
    {
      personalizations: [ recipient_list ],
      from: {email: sender},
      subject: email_subject,
      content: [{type: 'text/plain', value: message}]
    }.to_json
  end

  subject { SendGridClient.new(url: url, api_key: api_key) }

  describe '#payload' do
    context 'when minimal message config is provided' do
      it { expect(subject.payload(message_config)).to eql(expected_payload) }
    end

    context 'when multiple recipients are passed' do
      let(:recipient) { "#{email1},#{email2}" }
      let(:expected_to_list) { [{email: email1}, {email: email2}] }

      it { expect(subject.payload(message_config)).to eql(expected_payload) }
    end

    context 'with bcc' do
      let(:bcc) { email1 }
      let(:config_with_bcc) { message_config.merge(bcc: bcc) }
      let(:expected_bcc_list) { [{email: email1}] }
      let(:recipient_list) do
        {
          to: expected_to_list,
          bcc: expected_bcc_list
        }
      end

      context 'with single email' do
        it { expect(subject.payload(config_with_bcc)).to eql(expected_payload) }
      end

      context 'with multiple email' do
        let(:bcc) { "#{email1},#{email2}" }
        let(:expected_bcc_list) do
          [
            {email: email1},
            {email: email2}
          ]
        end

        it { expect(subject.payload(config_with_bcc)).to eql(expected_payload) }
      end
    end

    context 'with cc' do
      let(:cc) { email1 }
      let(:config_with_cc) { message_config.merge(cc: cc) }
      let(:expected_cc_list) { [{email: email1}] }
      let(:recipient_list) do
        {
          to: expected_to_list,
          cc: expected_cc_list
        }
      end

      context 'with single email' do
        it { expect(subject.payload(config_with_cc)).to eql(expected_payload) }
      end

      context 'with multiple email' do
        let(:cc) { "#{email1},#{email2}" }
        let(:expected_cc_list) do
          [
            {email: email1},
            {email: email2}
          ]
        end

        it { expect(subject.payload(config_with_cc)).to eql(expected_payload) }
      end
    end

  end
end
