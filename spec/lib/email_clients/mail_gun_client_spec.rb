require 'rails_helper'

RSpec.describe MailGunClient do
  let(:user) { 'api' }
  let(:password) { 'password' }
  let(:url) { 'http://api.endpoint.com' }
  let(:recipient) { 'recipient@email.com' }
  let(:email1) { 'email1@test.com' }
  let(:email2) { 'email2@test.com' }
  let(:sender) { 'sender@email.com' }
  let(:email_subject) { 'Hello user' }
  let(:message) { 'Hello world!' }

  let(:message_config) do
    {
      to: recipient,
      from: sender,
      subject: email_subject,
      message: message
    }
  end

  let(:expected_payload) do
    {
      from: sender,
      to: recipient,
      subject: email_subject,
      text: message
    }
  end

  subject { MailGunClient.new(url: url, user: user, password: password) }

  describe '#payload' do
    context 'when minimal message config is provided' do
      it { expect(subject.payload(message_config)).to eql(expected_payload) }
    end

    context 'when multiple recipients are passed' do
      let(:recipient) { "#{email1},#{email2}" }

      it { expect(subject.payload(message_config)).to eql(expected_payload) }
    end

    context 'with bcc' do
      let(:bcc) { 'email@test.com' }
      let(:expected_payload_with_bcc) { expected_payload.merge(bcc: bcc) }
      let(:config_with_bcc) { message_config.merge(bcc: bcc) }

      context 'with single email' do
        it { expect(subject.payload(config_with_bcc)).to eql(expected_payload_with_bcc) }
      end

      context 'with multiple email' do
        let(:bcc) { "#{email1},#{email2}" }

        it { expect(subject.payload(config_with_bcc)).to eql(expected_payload_with_bcc) }
      end
    end

    context 'with cc' do
      let(:cc) { 'email1@test.com' }
      let(:expected_payload_with_cc) { expected_payload.merge(cc: cc) }
      let(:config_with_cc) { message_config.merge(cc: cc) }

      context 'with single email' do
        it { expect(subject.payload(config_with_cc)).to eql(expected_payload_with_cc) }
      end

      context 'with multiple email' do
        let(:cc) { "#{email1},#{email2}" }

        it { expect(subject.payload(config_with_cc)).to eql(expected_payload_with_cc) }
      end
    end

  end
end
