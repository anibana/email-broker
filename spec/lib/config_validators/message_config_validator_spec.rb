require 'rails_helper'

RSpec.describe MessageConfigValidator do
  let(:recipient) { 'recipient@email.com' }
  let(:sender) { 'sender@email.com' }
  let(:email_subject) { 'Hello user!' }
  let(:message) { 'Hello world!' }
  let(:config) do
    {
      to: recipient,
      from: sender,
      subject: email_subject,
      message: message
    }
  end

  subject { MessageConfigValidator.new(config) }

  describe '#validate!' do
    context 'when minimal config is valid' do
      it { expect{subject.validate!}.to_not raise_error }
    end

    describe "'to' field" do
      context 'when it is not provided' do
        let(:config) do
          {
            from: sender,
            subject: email_subject,
            message: message
          }
        end

        it { expect{subject.validate!}.to raise_error(MessageConfigValidator::InvalidMessageError, "'to' is required") }
      end

      context 'using invalid email' do
        let(:recipient) { 'invalid-email.com' }

        it { expect{subject.validate!}.to raise_error(MessageConfigValidator::InvalidMessageError, "'to' is not using valid email format") }
      end

      context 'when one email is invalid' do
        let(:recipient) { 'valid@email.com,invalid-email.com' }

        it { expect{subject.validate!}.to raise_error(MessageConfigValidator::InvalidMessageError, "'to' is not using valid email format") }
      end
    end

    describe "'bcc' field" do
      context 'when it is not provided' do
        let(:config) do
          {
            to: recipient,
            from: sender,
            subject: email_subject,
            message: message
          }
        end

        it { expect{subject.validate!}.to_not raise_error }
      end

      context 'when it is provided' do
        let(:bcc) { 'valid@email.com' }
        let(:config) do
          {
            to: recipient,
            from: sender,
            subject: email_subject,
            message: message,
            bcc: bcc
          }
        end

        it { expect{subject.validate!}.to_not raise_error }

        context 'using invalid email' do
          let(:bcc) { 'invalid-email.com' }

          it { expect{subject.validate!}.to raise_error(MessageConfigValidator::InvalidMessageError, "'bcc' is not using valid email format") }
        end

        context 'when one email is invalid' do
          let(:bcc) { 'valid@email.com,invalid-email.com' }

          it { expect{subject.validate!}.to raise_error(MessageConfigValidator::InvalidMessageError, "'bcc' is not using valid email format") }
        end
      end
    end

    describe "'cc' field" do
      context 'when it is not provided' do
        let(:config) do
          {
            to: recipient,
            from: sender,
            subject: email_subject,
            message: message
          }
        end

        it { expect{subject.validate!}.to_not raise_error }
      end

      context 'when it is provided' do
        let(:cc) { 'valid@email.com' }
        let(:config) do
          {
            to: recipient,
            from: sender,
            subject: email_subject,
            message: message,
            cc: cc
          }
        end

        it { expect{subject.validate!}.to_not raise_error }

        context 'using invalid email' do
          let(:cc) { 'invalid-email.com' }

          it { expect{subject.validate!}.to raise_error(MessageConfigValidator::InvalidMessageError, "'cc' is not using valid email format") }
        end

        context 'when one email is invalid' do
          let(:cc) { 'valid@email.com,invalid-email.com' }

          it { expect{subject.validate!}.to raise_error(MessageConfigValidator::InvalidMessageError, "'cc' is not using valid email format") }
        end
      end
    end

    describe "'from' field" do
      context 'when it is not provided' do
        let(:config) do
          {
            to: recipient,
            subject: email_subject,
            message: message
          }
        end

        it { expect{subject.validate!}.to raise_error(MessageConfigValidator::InvalidMessageError, "'from' is required") }
      end

      context 'using invalid email' do
        let(:sender) { 'invalid-email.com' }

        it { expect{subject.validate!}.to raise_error(MessageConfigValidator::InvalidMessageError, "sender email 'from' is not a valid email") }
      end
    end

    context "'subject' field" do
      context 'when it is not provided' do
        let(:config) do
          {
            from: sender,
            to: recipient,
            message: message
          }
        end

        it { expect{subject.validate!}.to raise_error(MessageConfigValidator::InvalidMessageError, "'subject' is required") }
      end
    end

    context "'message' field" do
      context 'when it is not provided' do
        let(:config) do
          {
            from: sender,
            to: recipient,
            subject: email_subject
          }
        end

        it { expect{subject.validate!}.to raise_error(MessageConfigValidator::InvalidMessageError, "'message' is required") }
      end
    end

  end
end
