require 'rails_helper'

RSpec.describe ProviderConfigValidator do
  describe "#validate!" do
    let(:url) { 'https://api.endpoint.com' }
    let(:api_key) { 'api-key-here' }
    let(:user) { 'api' }
    let(:password) { 'password' }
    let(:config) do
      {
        url: url,
        api_key: api_key
      }
    end

    subject { ProviderConfigValidator.new(config).validate! }

    context 'when api_key and url is provided' do
      it { expect{subject}.to_not raise_error }
    end

    context 'when user, password, and url is provided' do
      let(:config) { {user: user, password: password, url: url} }
      it { expect{subject}.to_not raise_error }
    end

    context 'when url is not passed' do
      let(:config) { {api_key: api_key} }
      it { expect{subject}.to raise_error(ProviderConfigValidator::InvalidConfigError) }
    end

    context 'when user is provided but not password' do
      let(:config) { { url: url, user: user } }
      it { expect{subject}.to raise_error(ProviderConfigValidator::InvalidConfigError) }
    end

    context 'when password is provided but not user' do
      let(:config) { { url: url, password: password } }
      it { expect{subject}.to raise_error(ProviderConfigValidator::InvalidConfigError) }
    end
  end
end
