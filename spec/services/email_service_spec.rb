require 'rails_helper'

RSpec.describe EmailService do
  let(:recipient) { 'recipient@email.com' }
  let(:sender) { 'sender@email.com' }
  let(:email_subject) { 'Hello user!' }
  let(:message) { 'Hello world!' }
  let(:config) do
    {
      to: recipient,
      from: sender,
      subject: email_subject,
      message: message
    }
  end

  let(:send_grid) { double 'send_grid' }
  let(:mail_gun) { double 'mail_gun' }

  subject { EmailService.new(config, [send_grid, mail_gun]) }

  before do
    allow(subject.validator).to receive(:validate!).and_return(true)
    allow(mail_gun).to receive(:send!)
    allow(send_grid).to receive(:send!)
  end

  describe '#send!' do
    context 'when both of the email providers are working' do
      it 'sends message' do
        subject.send!
        expect(subject.sent?).to eql(true)
      end
    end

    context 'when mail_gun is not working' do
      it 'still sends message' do
        allow(mail_gun).to receive(:send!).and_raise("service down")
        subject.send!
        expect(subject.sent?).to eql(true)
      end
    end

    context 'when send_grid is not working' do
      it 'still sends message' do
        allow(send_grid).to receive(:send!).and_raise("service down")

        subject.send!
        expect(subject.sent?).to eql(true)
      end
    end

    context 'when both providers are not working' do
      it 'will not send the message and sets error' do
        allow(mail_gun).to receive(:send!).and_raise("service down")
        allow(send_grid).to receive(:send!).and_raise("service down")

        subject.send!
        expect(subject.sent?).to eql(false)
        expect(subject.error_message).to eql('service down')
      end
    end

    context 'validator raises an error' do
      let(:config) { {to: recipient} }

      before { allow(subject.validator).to receive(:validate!).and_raise("invalid config") }
      it { expect{subject.send!}.to raise_error(RuntimeError, "invalid config")}
    end

  end
end
