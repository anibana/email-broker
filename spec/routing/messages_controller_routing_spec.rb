require 'rails_helper'

RSpec.describe MessagesController, type: :routing do
  it { expect(post('messages')).to route_to('messages#create') }

  it { expect(get('messages')).to_not route_to('messages#index') }
  it { expect(get('messages/1')).to_not route_to('messages#show', id: '1') }
  it { expect(get('messages/new')).to_not route_to('messages#new') }
  it { expect(get('messages/1/edit')).to_not route_to('messages#edit', id: '1') }
  it { expect(put('messages/1')).to_not route_to('messages#update', id: '1') }
  it { expect(delete('messages/1')).to_not route_to('messages#destroy', id: '1') }
end
