require 'rails_helper'

RSpec.describe MessagesController, type: :request do
  let(:params) do
    {
      to: 'recipient@email.com',
      from: 'sender@email.com',
      subject: 'Hello user!',
      message: 'Hello world',
      format: :json
    }
  end

  describe 'POST /messages' do
    before do
      allow_any_instance_of(EmailService).to receive(:send!).and_return(true)
      allow_any_instance_of(EmailService).to receive(:sent?).and_return(true)
    end

    subject { post '/messages', params: params }

    context 'when request params is valid' do
      it 'renders sent template' do
        subject

        expect(response).to render_template(:sent)
        expect(response.code).to eql('202')
      end
    end

    context 'when request params is not valid' do
      it 'renders input_error template' do
        allow_any_instance_of(EmailService).to receive(:send!).and_raise(MessageConfigValidator::InvalidMessageError)

        subject

        expect(response).to render_template(:input_error)
        expect(response.code).to eql('422')
      end
    end

    context 'when email is not sent using any providers' do
      it 'renders failed template' do
        allow_any_instance_of(EmailService).to receive(:sent?).and_return(false)

        subject

        expect(response).to render_template(:failed)
        expect(response.code).to eql('500')
      end
    end
  end
end
