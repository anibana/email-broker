class ProviderConfigValidator
  class InvalidConfigError < StandardError; end

  attr_reader :config

  def initialize(config)
    @config = config
  end

  def validate!
    raise InvalidConfigError unless config[:url] && (config[:api_key] || (config[:user] && config[:password]))
  end
end
