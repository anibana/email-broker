class MessageConfigValidator
  class InvalidMessageError < StandardError; end

  attr_reader :message_config, :errors

  def initialize(message_config)
    @message_config = message_config
    @errors = []
  end

  def validate!
    validate_to
    validate_from
    validate_bcc
    validate_cc
    validate_subject
    validate_message

    raise InvalidMessageError, error_message if errors.any?
  end

  private
  def validate_to
    errors << "'to' is required" unless message_config[:to].present?
    errors << "'to' is not using valid email format" if message_config[:to].present? && !valid_email_list?(message_config[:to])
  end

  def validate_from
    errors << "'from' is required" unless message_config[:from].present?
    errors << "sender email 'from' is not a valid email" if message_config[:from].present? && !valid_email?(message_config[:from])
  end

  def validate_bcc
    if message_config[:bcc].present? && !valid_email_list?(message_config[:bcc])
      errors << "'bcc' is not using valid email format"
    end
  end

  def validate_cc
    if message_config[:cc].present? && !valid_email_list?(message_config[:cc])
      errors << "'cc' is not using valid email format"
    end
  end

  def validate_subject
    errors << "'subject' is required" unless message_config[:subject].present?
  end

  def validate_message
    errors << "'message' is required" unless message_config[:message].present?
  end

  def error_message
    errors.join(', ')
  end

  def valid_email_list?(list)
    list.split(',').all? { |email| valid_email?(email) }
  end

  def valid_email?(email)
    email =~ /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  end
end
