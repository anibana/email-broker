class SendGridClient < BaseEmailClient
  def payload(message_config)
    recipients_list = extract_recipients(message_config)

    {
      personalizations: [ recipients_list ],
      from: {email: message_config[:from]},
      subject: message_config[:subject],
      content: [{type: 'text/plain', value: message_config[:message]}]
    }.to_json
  end

  private

  def extract_recipients(message_config)
    to_list = build_email_list(message_config[:to])
    bcc_list = build_email_list(message_config[:bcc])
    cc_list = build_email_list(message_config[:cc])

    recipients = { to: to_list }
    recipients.merge!(bcc: bcc_list) if bcc_list.any?
    recipients.merge!(cc: cc_list) if cc_list.any?
    recipients
  end

  def build_email_list(list)
    return list.split(',').map {|email| { email: email }} if list.present?
    []
  end
end
