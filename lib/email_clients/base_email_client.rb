class BaseEmailClient
  def initialize(config={})
    @password = config[:password]
    @user = config[:user]
    @url = config[:url]
    @api_key = config[:api_key]
  end

  def send!(message_config)
    RestClient.post url,
      payload(message_config),
      headers
  end

  def payload(message_config)
    raise NotImplementedError, "#payload method has not been implemented in #{self.class}"
  end

  protected

  def headers
    return { Authorization: "Bearer #{@api_key}", content_type: :json } if @api_key.present?
    {}
  end

  def url
    return @url if @api_key.present?

    "https://#{@user}:#{@password}@#{@url}"
  end
end
