class MailGunClient < BaseEmailClient
  def payload(message_config)
    body = {
      from: message_config[:from],
      to: message_config[:to],
      subject: message_config[:subject],
      text: message_config[:message]
    }

    body.merge!(bcc: message_config[:bcc]) if message_config[:bcc].present?
    body.merge!(cc: message_config[:cc]) if message_config[:cc].present?

    body
  end
end
