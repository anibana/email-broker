class EmailService
  attr_reader :id, :error_message, :validator

  def initialize(message_config, providers=EMAIL_PROVIDERS)
    @message_config = message_config
    @providers = providers
    @id = SecureRandom.uuid
    @validator = MessageConfigValidator.new(@message_config)
  end

  def send!
    validator.validate!

    @providers.each_with_index do |provider, index|
      begin
        provider.send!(@message_config)
        sent_successful and break
      rescue => e
        @error_message = e.message
        Rails.logger.error "Email sending failed using #{provider.class}, error=\"#{e}\""
        Rails.logger.info "Trying next provider.." unless index == @providers.count - 1
      end
    end
  end

  def sent?
    @successful || false
  end

  private

  def sent_successful
    @successful = true
  end
end
