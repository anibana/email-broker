class MessagesController < ApplicationController
  def create
    begin
      @email_message = EmailService.new(message_params)
      @email_message.send!

      if @email_message.sent?
        Rails.logger.info "Email #{@email_message.id} sent successfully"
        render :sent, status: :accepted
      else
        Rails.logger.error "Both of providers failed to send the email, id=#{@email_message.id} error=\"#{@email_message.error_message}\""
        render :failed, status: :internal_server_error
      end
    rescue MessageConfigValidator::InvalidMessageError => e
      @error_message = e.message
      Rails.logger.info "Invalid user input, id=#{@email_message.id} error=\"#{@error_message}\""

      render :input_error, status: :unprocessable_entity
    end
  end

  protected

  def message_params
    params.permit(:to, :from, :bcc, :cc, :subject, :message)
  end
end
