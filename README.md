# Email Broker API

## Problem
Create a service (backend) that accepts the necessary information and sends emails.

The application should provide an abstraction between two different email service providers. If one of the services goes down, your service can quickly failover to a different provider without affecting your customers.

Email Providers:
* [Mailgun](https://www.mailgun.com/) [Simple Send Documentation](https://documentation.mailgun.com/en/latest/api-sending.html)
* [SendGrid](https://sendgrid.com/) - [Simple Send Documentation](https://sendgrid.com/docs/API_Reference/Web_API_v3/index.html)

Your solution should cater for multiple email recipients, CCs and BCCs but there is no need to support HTML email body types (plain text is OK)

## Solution
### Automatic email provider client creation - `config/initializers/email_providers.rb`
Upon initial bootup, the application will read list of email providers
with their configuration. It will also instantiate corresponding Client
wrappers.

You can add as many email providers as you want as long as you have
implemented the corresponding email wrapper (located in
`lib/email_clients/`)

### Email failover - `app/services/email_service.rb`
Round robin solution - it will try to send the message for each email
providers. If one of the providers has
successfully sent the messsage, it will stop trying to send the same
message for the other providers.

### Creating new Email Provider Client
Extend `BaseEmailClient` `lib/email_clients/base_email_client.rb` and
make sure the the filename and class name have the correct formats. If
not, the inititalizer wont be able to load the client correctly .

Example:

`mandrill` - specified in `email_providers.yml`

Class name: `MandrillClient`

filename: `mandrill_client.rb`

## Deployed to Heroku
https://email-broker.herokuapp.com/messages

## System dependencies
* Ruby 2.3.1
* Ruby on Rails 5.0.4

## How to run locally
* Install dependencies, execute `bundle install`
* Run server locally, execute `bundle exec rails s`

## How to run specs
Execute `bundle exec rspec`

## How to use
You can use curl or Postman to test this API.

### POST /messages

#### Parameters
* `to` (required) - email recipients. You can add multiple emails, just use comma "," as a separator. E.g. `email1@test.com,email2@test.com`
* `from` (required) - email sender. E.g. `sender@email.com`
* `bcc` (optional) - bcc recipients. You can add multiple emails, just use comma "," as a separator. E.g. `email1@test.com,email2@test.com`
* `cc` (optional) - cc recipients. You can add multiple emails, just use comma "," as a separator. E.g. `email1@test.com,email2@test.com`
* `subject` (required) - email subject
* `message` (required) - email body. `Text` format is the only format supported for now.

Sample curl Request:
```
curl --data "to=test@email.com&from=sender@email.com&subject=Your subject&message=Hello world&bcc=email1@test.com,email2@test.com&cc=email3@test.com" -X POST https://email-broker.herokuapp.com/messages
```

## Email Provider Configuration
Email provider configuration is located at `config/email_providers.yml`.

`Send Grid` and `Mail Gun` are the only email providers supported right now.

If you need to add another email provider, just add the provider name to the list with the correct configuration.

Example:
```yaml
PROVIDERS:
  send_grid:
    api_key: SEND-GRID-API-KEY
    url: SEND-GRID-URL

  mail_gun:
    user: MAIL-GUN-USER
    password: MAIL-GUN-PASSWORD
    url: MAIL-GUN-URL

  some_other_provider
    user: PROVIDER_USER
    password: PROVIDER_PASSWORD
    url: PROVIDER_URL
```

## Gems used
* [RSpec](http://rspec.info/)
* [RestClient](https://github.com/rest-client/rest-client)

## Things to add
* Improve logging.
* Edge error handling.
* Additional error response message.
* Add Swagger API Documentation.

## Notes
I know pushing configuration to repo is a bad practice. I just pushed it for the
ease of setting it up locally. You can change it to your own email
provider configurations. :)
